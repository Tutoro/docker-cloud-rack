# Wallabag

https://wallabag.org

## Configure
```
echo POSTGRES_PASSWORD=$(openssl rand -base64 32) >> .env
echo SYMFONY__ENV__DATABASE_PASSWORD=$(openssl rand -base64 32) >> .env
echo SYMFONY__ENV__SECRET=$(openssl rand -base64 30) >> .env
echo SYMFONY__ENV__MAILER_HOST=changeme >> .env
echo SYMFONY__ENV__MAILER_USER=changeme >> .env
echo SYMFONY__ENV__FROM_EMAIL=changeme >> .env
echo SYMFONY__ENV__MAILER_PASSWORD=changeme >> .env
```

If you want to test with **http** instead of **https**, just export the following variable before launching `docker compose`:
```
# when not exported, default to https
export PROTOCOL=http
```

## Deploy
```
docker compose up -d
```

## First login

Go to your browser and login with wallabag/wallabag
**DO NOT FORGET** to change the user & password after first login

## Migration from Postgres 10 to 14

- First backup DB in version 10
```
docker compose exec -T db sh -c 'PGPASSWORD="${POSTGRES_PASSWORD}" pg_dumpall -c -U ${POSTGRES_USER}' > "./db_v10.sql"
```
- IMPORTANT: Check the content of the SQL file to make sure it is not empty
```
docker compose down
```
- Here you should backup all the directories of your kanboard application
```
rm -rf ${CHATONS_ROOT_DIR}/${CHATONS_SERVICE}/db
docker compose up -d db
```
- Check in the docker logs that the DB is up and running. You should find something like this: database system is ready to accept connections
```
docker compose exec -T db sh -c 'PGPASSWORD="${POSTGRES_PASSWORD}" psql -U ${POSTGRES_USER} template1' < ./db_v10.sql
docker compose rm -s -f db
docker compose up -d
```

- Now you need to connect to the DB
```
docker-compose exec db sh
```
- You are now inside the DB container
```
PGPASSWORD="${POSTGRES_PASSWORD}" psql postgres postgres
select * from pg_shadow;
```

- It will show you normally 2 lines for the the postgres and wallabag user (if you have more users, you need to perform the next command for each of them)
```
ALTER ROLE postgres WITH PASSWORD 'put here the password of the postgres user'
ALTER ROLE wallabag WITH PASSWORD 'put here the password of the wallabag user'
```

- Check that the change is properly done
```
select * from pg_shadow;
```
- you should now see the passwd field starting with SCRAM-SHA-256$4096:
- you might have to restart the service with
```
docker compose down
docker compose up -d
```
