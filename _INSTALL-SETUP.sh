#!/bin/bash

#SNIPPETS###
#sed -i "s/.*//" etc/ssh/sshd_config

read -r -p "Do you want to DELETE old Docker-Installation and Data? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
chmod g+x xscripts/_uninstall-docker_and_delete-data.sh
bash xscripts/_uninstall-docker_and_delete-data.sh || exit 1
else
echo "OK"
fi


echo ""
echo "##############################"
echo "# YOUR SERVER FINGERPRINT IS #"
echo "##############################"
echo ""
#Server Fingerprint mit localem vergleichen 
echo $(ssh-keygen -l -E md5 -f /etc/ssh/ssh_host_ed25519_key) > ~/fingerprint.txt
cat ~/fingerprint.txt

echo ""
echo ""
echo "######################################################"
echo "# MAKE YOUR CONFIGURATIONS FOR THE DOCKER-CLOUD-RACK #"
echo "######################################################"
echo ""

echo "Please enter your MAINDOWMAIN (domain.tld [CHATONS_DOMAIN]):"
read MAINDOMAIN
echo "export CHATONS_DOMAIN=${MAINDOMAIN}" >> ~/.bash_aliases
echo "export ACME_EMAIL=acme@${MAINDOMAIN}" >> ~/.bash_aliases
. ~/.bash_aliases

echo ""
echo ""
echo "#########################################################"
echo "# INSTALLING REQUIRED PACKAGES, DOCKER & DOCKER-COMPOSE #"
echo "#########################################################"
echo ""
#Update
apt update
apt -qq upgrade -y


###Define Architecture as string
arch=$(dpkg --print-architecture)

### ARM64
if [ "$arch" == 'arm64' ];
then 
echo ""
echo ""
echo "#################################################################"
echo "# ARM64 - INSTALLING REQUIRED PACKAGES, DOCKER & DOCKER-COMPOSE #"
echo "#################################################################"
echo "" 
#ínstall Docker
apt-get -qq install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=arm64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update

apt -qq install docker-ce docker-ce-cli containerd.io -y


#install docker-compose
apt -qq install -y python3-pip libffi-dev
sleep 5
yes | pip3 install docker-compose

apt -qq autoremove -y
else
echo "OK"
fi

###AMD64
if [ "$arch" == 'amd64' ];
then
echo ""
echo ""
echo "#################################################################"
echo "# X64 - INSTALLING REQUIRED PACKAGES, DOCKER & DOCKER-COMPOSE #"
echo "#################################################################"
echo "" 
#https://docs.docker.com/engine/install/ubuntu/
apt install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     git \
     gnupg \
     gnupg2 \
     software-properties-common \
     lsb-release


#https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
mkdir -p /etc/apt/keyrings

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -



chmod a+r /etc/apt/keyrings/docker.gpg

apt update
apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin python3-pip unzip htop nmon tree screen -y
else
echo "OK"
fi

echo "If you want to use the Framagit-Ateilier type number 1"
echo "If you want to download your own https-Ateilier type number 2"
echo "If you already have a Ateilier, you want to use, type number 3 "
read -p "Enter the maching NUMBER: " USER_INPUT
if [[ $USER_INPUT == "1" ]]; then
echo "Please enter directory with ABSOLUTE path !!!WITHOUT LAST SLASH!!! to place the Framagit-atelier-files: "
read  ATELIER
mkdir $ATELIER
git clone --recursive https://framagit.org/oxyta.net/docker-atelier.git $ATELIER
echo "Enter your Root-Path: "
echo "export CHATONS_ROOT_DIR=${ATELIER}" >> ~/.bash_aliases
. ~/.bash_aliases
	 
elif [[ $USER_INPUT == "2" ]]; then
echo "Please enter directory with ABSOLUTE path !!!WITHOUT LAST SLASH!!! to place the atelier-files: "
read  ATELIER
mkdir $ATELIER
echo "Please enter your own https Git-URL: "
read GIT_URL
git clone --recursive ${GIT_URL} $ATELIER 
echo "export CHATONS_ROOT_DIR=${ATELIER}" >> ~/.bash_aliases
. ~/.bash_aliases
cp -R ./proxy/prod-le $ATELIER/proxy/
elif [[ $USER_INPUT == "3" ]]; then
echo "Please enter the ABSOLUTE path without last slash where your files are: "
read  ATELIER
echo "export CHATONS_ROOT_DIR=${ATELIER}" >> ~/.bash_aliases
. ~/.bash_aliases

fi


#Traefik-Proxy configuration with Let's Encrypt certificates


#read -p "Please enter Mail for Letsencrypt (your@email.address): " LETSMAIL
echo ACME_EMAIL=acme@${MAINDOMAIN}  >> $ATELIER/proxy/prod-le/.env
#read -p "Please enter Domain and TLD for the System (maindomain.tld): " MAINDOMAIN
echo CHATONS_DOMAIN=$MAINDOMAIN >> $ATELIER/proxy/prod-le/.env
#create username & password for Traefik-Proxy
read -p "Traefik-Proxy Webinterface-Username: "  USER1
htdigest -c $ATELIER/proxy/prod-le/htdigest traefik $USER1

touch $ATELIER/proxy/prod-le/acme.json htdigest
chmod 600 $ATELIER/proxy/prod-le/acme.json

sleep 5

# Start docker
systemctl start docker
systemctl enable docker
systemctl start docker
systemctl enable docker
docker network create --ipv6 --subnet=fd00:dead:beef::/48 web

cp _INSTALL-SETUP.sh $ATELIER
cp -R ./xscripts $ATELIER
chmod ug+x $ATELIER/xscripts/*.sh

docker-compose -f $ATELIER/proxy/prod-le/docker-compose.yml up -d


#Configure Nextcloud
read -r -p "Do you want to configure Nextcloud? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
echo POSTGRES_PASSWORD=$(openssl rand -base64 32) >> /cloud/.env
else
echo "OK, Nextcloud not configured!"
fi



read -r -p "You want to install NoIP-Client? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
#Dyndns Account erstellen
#https://www.noip.com 
#Dienst installieren und einrichten
cd /usr/local/src/
sudo wget http://www.noip.com/client/linux/noip-duc-linux.tar.gz -P 
sudo tar xf /usr/local/src/noip-duc-linux.tar.gz
sudo apt-get install make cc
sudo /usr/local/src/noip-2.1.9-1/ make install
sudo /usr/local/bin/noip2 –C
sudo /usr/local/bin/noip2
else
echo "OK, no NoIP"
fi



#RAPBERRY PI disable Bluetooth
read -r -p "If you set it up on a Raspberry PI, do you want to disable Bluetooth? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
sed -i "s/dtoverlay=.*/dtoverlay=disable-bt/" /boot/config.txt
else
echo "OK, Bluetooth stays activated!"
fi



echo ""
echo ""
echo "###################################################"
echo "# CONGRATULATION THE INSTALLATION IS NOW FINISHED #"
echo "###################################################"
echo "" 
echo ""
echo "YOU CAN NOW ENTER THE WEBINTERFACE AT: https://traefik.${MAINDOMAIN}"
echo ""
echo ""
echo "DONT PANIC, IT CAN TAKE 2 MINUTES UNTIL TRAEFIK IS REACHABLE"
echo ""

exit 0
