#!/bin/bash

read -r -p "Want to DELETE the .bash_aliases? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
echo ""
echo "##################################"
echo "# DELETING THE BASH_ALIASES FILE #"
echo "##################################"
echo ""
rm ~/.bash_aliases
else
echo "NO"
fi

read -r -p "Want to DELETE all Docker Containers/Images/Networks? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
echo ""
echo "########################################################"
echo "# DELETING ALL DOCKER DATA LIKE CONTAINERS AND NETWORK #"
echo "########################################################"
echo "OK"
docker stop $(docker ps -a -q)
docker container prune
docker image prune
docker network prune
docker system prune
else
echo "NO"
fi

###Define Architecture as string
arch=$(dpkg --print-architecture)
echo "${arch}"

### ARM64
if [ "$arch" == 'arm64' ];
then 
echo ""
echo "################################"
echo "# UNISTALLING ALL DOCKER STUFF #"
echo "################################"
echo ""

#https://docs.docker.com/engine/install/ubuntu/#uninstall-docker-engine
#Remove older Versions of Docker
apt -qq remove docker-ce docker-ce-cli containerd.io python3-pip libffi-dev -y

#If you also want to clean up the data
apt -qq -y purge docker-ce docker-ce-cli containerd.io python3-pip libffi-dev
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
apt -qq autoremove -y
apt -qq autoclean -y
else
echo "OK"
fi

###AMD64
if [ "$arch" == 'amd64' ];
then
echo ""
echo "################################"
echo "# UNISTALLING ALL DOCKER STUFF #"
echo "################################"
echo ""

#https://docs.docker.com/engine/install/ubuntu/#uninstall-docker-engine
#Remove older Versions of Docker
apt -qq remove docker docker.io containerd runc docker-engine -y

#If you also want to clean up the data
apt -qq -y purge docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-ce-rootless-extras
rm -rf /var/lib/docker
rm -rf /var/lib/containerd
apt -qq autoremove -y
apt -qq autoclean -y
else
echo "OK"
fi




read -r -p "Do you now want to reinstall Docker and Stuff? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
bash ./_INSTALL-SETUP.sh
else
exit 1
fi
