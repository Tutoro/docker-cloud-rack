read -r -p "You want an new UNIX-USER for the Docker-Cloud-Rack?? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
read -p "Please enter Username for the NEW Unix-User: " UNIX_USER

#Benutzer anlegen
adduser ${UNIX_USER}

#Angelegten Nutzer zu sudo Gruppe hinzugügen
usermod -aG sudo ${UNIX_USER}

#Nach Login Benutzer wechseln
su -  ${UNIX_USER}
else
 
fi

###################Enable Password configuration
read -r -p "You want a Secure-User without Home and Rights and only allow him to connect SSH? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
#SSH SecureUser anlegen
useradd login

#SSH absichern (folgendes ändern oder hinzufügen)

read -p "Do you want to change the SSH-Port? So please : " SSH_PORT
sed -i "s/#Port.*/Port ${SSH_PORT}/" etc/ssh/sshd_config
sed -i "s/PermitRootLogin.*/PermitRootLogin no/" etc/ssh/sshd_config
sed -i "s/MaxAuthTries.*/MaxAuthTries 3/" etc/ssh/sshd_config
sed -i "s/MaxSessions.*/MaxSessions 6/" etc/ssh/sshd_config
sed -i "s/AllowUsers.*/AllowUsers ${UNIX_USER}/" etc/ssh/sshd_config

/etc/init.d/ssh restart
else
 
fi


read -r -p "You want to install UbuntuFireWall and configure? (Y/N): " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
 #Firewall einrichten
sudo apt-get install ufw

sudo ufw allow http
sudo ufw allow https

#SSH Port freigeben
sudo ufw allow 2020/tcp

sudo ufw app list

ufw logging medium

ufw default deny incoming

#Erst wenn alle Regeln 100% korrekt sind, die Firewall aktivieren
sudo ufw enable
else

fi