# Special update instructions

## Postgres

When something refuse to start because of a wrong postgres version, here are the step to update it:

1. `docker compose down`
2. double check the `image` version of the `db` service: it must be the one that used to work for you
3. add `/srv/chatons/<service>/upgrade:/upgrade` to the `volumes` section of the `db` service
4. `docker compose up -d db`
5. `docker compose exec db sh`
6. `pg_dumpall -U $POSTGRES_USER > /upgrade/dump.sql`
7. `exit` from the db container
8. `docker compose down`
9. `sudo mv /srv/chatons/<service>/postgres /srv/chatons/<service>/old-postgres`
10. update the `image` version of the `db` service back: it must be the the new one
11. `docker compose up -d db`
12. `docker compose exec db sh`
13. `psql -U $POSTGRES_USER < /upgrade/dump.sql`
14. `exit` from the db container
15. remove `/srv/chatons/<service>/upgrade:/upgrade` to the `volumes` section of the `db` service
16. you should be able to restart everything with `docker-compose up -d`
17. If everything works well, you can remove the `upgrade` and the `old-postgres` folders
