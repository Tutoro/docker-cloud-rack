# Keycloak

https://www.keycloak.org/

## Configure

```
export DB_PASSWORD=$(openssl rand -base64 32)
echo POSTGRES_PASSWORD=$DB_PASSWORD >> .env
echo KC_DB_PASSWORD=$DB_PASSWORD >> .env
echo KEYCLOAK_ADMIN=admin >> .env
echo KEYCLOAK_ADMIN_PASSWORD=$(openssl rand -base64 32) >> .env
```

## Deploy
```
docker compose up -d
```

## Use

With your admin account, create a new realm ("master" is only for keycloak), and select it.

### With Nextcloud

TODO

### With Peertube

TODO
