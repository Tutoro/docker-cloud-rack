# Nextcloud

https://nextcloud.com/

## Configure
```
echo POSTGRES_PASSWORD=$(openssl rand -base64 32) >> .env
```

if you want to change the `pm.max_children` value for nextcloud container (default value of official nextcloud container is untouched if variable is not exported):
```
echo NEXTCLOUD_MAX_CHILDREN=50 >> .env
```

## Deploy

```bash
docker compose up -d
```

Then you can connect to your Nextcloud instance to perform the installation.

If you prefer to install the instance in command line, you can launch the following commands (Replace variables below by their value ): 

Note: in order to run the following commands, you may need to manually install [jq](https://stedolan.github.io/jq), you can do so by running the following command on debian: `apt install jq`

```
docker compose exec --user www-data app php /var/www/html/occ maintenance:install --database "pgsql" --database-name "nextcloud"  --database-host="db" --database-user "nextcloud" --database-pass "$POSTGRES_PASSWORD" --admin-user "$ADMIN_USER" --admin-pass "$ADMIN_PASSWORD"
docker compose exec --user www-data app php occ config:system:set trusted_domains 0 --value=$CHATONS_SERVICE.$CHATONS_DOMAIN
docker compose exec --user www-data app php occ config:system:set overwrite.cli.url --value=http://$CHATONS_SERVICE.$CHATONS_DOMAIN
# to avoid error message related to untrusted proxies
idx=0
for ip_range in $(docker network inspect web | jq '.[].IPAM.Config[].Subnet');
do
  docker compose exec --user www-data app php occ config:system:set trusted_proxies ${idx} --value="${ip_range}"
  idx=$((idx+1))
done
# to ensure reset password is working fine behind proxy
docker compose exec --user www-data app php occ config:system:set  overwriteprotocol --value="https" --type="string"
```

## Deploy with scaling of the "app" container

```
docker compose up -d --scale app=3  # to get 3 instance of the "app" container
```

## Migration from Postgres 10 to 14

- First backup DB in version 10
```
# to ensure only DB is up and running
docker compose down
docker compose up -d db
docker compose exec -T db sh -c 'PGPASSWORD="${POSTGRES_PASSWORD}" pg_dumpall -c -U ${POSTGRES_USER}' > "./db_v10.sql"
```
- IMPORTANT: Check the content of the SQL file to make sure it is not empty
```
docker compose down
```
- Here you should backup all the directories of your Nextcloud application
```
rm -rf ${CHATONS_ROOT_DIR:-/srv/chatons}/${CHATONS_SERVICE:-cloud}/db
docker compose up -d db
```
- Check in the docker logs that the DB is up and running. You should find something like this: database system is ready to accept connections
```
docker compose exec -T db sh -c 'PGPASSWORD="${POSTGRES_PASSWORD}" psql -U ${POSTGRES_USER} template1' < ./db_v10.sql
docker compose up -d
```

- Now you need to connect to the DB
```
docker-compose exec db sh
```
- You are now inside the DB container
```
PGPASSWORD="${POSTGRES_PASSWORD}" psql nextcloud nextcloud
select * from pg_shadow;
```

- It will show you normally 2 lines for the the nextcloud and oc_xxxx user (if you have more users, you need to perform the next command for each of them)
```
ALTER ROLE oc_xxx WITH PASSWORD 'put here the password of the oc_xxx user (dbpassword value of config.php)'
ALTER ROLE nextcloud WITH PASSWORD 'put here the password of the nextcloud user (POSTGRES_PASSWORD value)'
```

- Check that the change is properly done
```
select * from pg_shadow;
```
- you should now see the passwd field starting with SCRAM-SHA-256$4096:
- you might have to restart the service with
```
docker compose down
docker compose up -d
```
